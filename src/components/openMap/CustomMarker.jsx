import React from 'react';
import L from 'leaflet';
import { Marker, Popup } from 'react-leaflet';

import mapPin from "../../assets/map-pin.svg"

export const CustomMarker = ({latitude, longitude}) => {
    // Custom Map Location Marker Icon
    const customIcon = L.icon({
        iconUrl: mapPin,
        iconSize: [32, 32],
        iconAnchor: [16, 32],
    })
    return(
        <Marker position={[latitude, longitude]} icon={customIcon}>
        <Popup>
          <p>
            Latitude: {latitude} Longitude: {longitude}
          </p>
        </Popup>
      </Marker>
    )
}