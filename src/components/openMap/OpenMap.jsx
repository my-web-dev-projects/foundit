import React from "react";

import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";

import "leaflet/dist/leaflet.css";
import { CustomMarker } from "./CustomMarker";

export const OpenMap = (props) => {
  const zoomLevel = 18;
  return (
    <div style={{ display: "flex", justifyContent: "center", padding:"10px" }}>
      <MapContainer
        center={[props.lat, props.lng]}
        zoom={zoomLevel}
        style={{ height: "50svh", width: "50svw" }}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <CustomMarker latitude={props.lat} longitude={props.lng}/>
      </MapContainer>
    </div>
  );
};
